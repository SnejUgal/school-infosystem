import React, { useState, useCallback } from "react";
import Button from "../Button";
import Dialog, { CloseButton } from "../Dialog";
import TextField from "../TextField";
import Class from "./Class";
import { EditedSchedule } from "./index";
import moment, { Moment } from "moment";
import { Ring } from "@infosystem/types/lib/ringSchedule";
import { ReactComponent as AddClassIcon } from "./icons/addClass.svg";

const ringToMoment = (ring: Partial<Ring>) =>
  moment()
    .startOf(`day`)
    .hour(ring.hour ?? 0)
    .minute(ring.minute ?? 0);

const isBetween = (previous: Moment, current: Moment, next: Moment) =>
  current.isBetween(previous, next, `minutes`, `()`);

const isScheduleValid = (schedule: EditedSchedule["schedule"]) => {
  let previousEnd = moment().subtract(1, `day`);

  return schedule.every((currentClass, index) => {
    const nextStart =
      index + 1 < schedule.length
        ? ringToMoment(schedule[index + 1].start)
        : moment().add(1, `day`);
    const start = ringToMoment(currentClass.start);
    const end = ringToMoment(currentClass.end);

    const isValid =
      isBetween(previousEnd, start, end) && isBetween(start, end, nextStart);

    previousEnd = end;

    return isValid;
  });
};

interface AddClassProps {
  onClick(): void;
}

const AddClass = ({ onClick }: AddClassProps) => {
  return (
    <li className="ringScheduleEditor_addClass">
      <button onClick={onClick}>
        <AddClassIcon />
      </button>
    </li>
  );
};

interface Props {
  editedSchedule: EditedSchedule;
  onScheduleEdit(schedule: EditedSchedule | null): void;
  onRemove(): void;
  onSave(): void;
  canBeDeleted: boolean;
  occupiedNames: string[];
}

const ScheduleEditor = ({
  editedSchedule,
  onScheduleEdit,
  onRemove,
  onSave,
  canBeDeleted,
  occupiedNames,
}: Props) => {
  const [recentClass, setRecentClass] = useState(-1);

  const addClass = (index: number) => {
    const newClass = {
      start: {},
      end: {},
      id: Math.max(...editedSchedule.schedule.map(({ id }) => id)) + 1,
    };
    const schedule = [...editedSchedule.schedule];
    schedule.splice(index, 0, newClass);
    onScheduleEdit({
      ...editedSchedule,
      schedule,
    });
    setRecentClass(schedule.length - 1);
  };

  const classes = editedSchedule.schedule
    .map((currentClass, index, schedule) => {
      const previousEnd =
        index > 0
          ? ringToMoment(schedule[index - 1].end)
          : moment().subtract(1, `day`);
      const nextStart =
        index + 1 < schedule.length
          ? ringToMoment(schedule[index + 1].start)
          : moment().add(1, `day`);
      const start = ringToMoment(currentClass.start);
      const end = ringToMoment(currentClass.end);
      const isStartValid = isBetween(previousEnd, start, end);
      const isEndValid = isBetween(start, end, nextStart);

      return [
        <AddClass key={`addClass-${index}`} onClick={() => addClass(index)} />,
        <Class
          key={`class-${currentClass.id}`}
          index={index}
          class={currentClass}
          onChange={newClass => {
            const schedule = [...editedSchedule.schedule];
            schedule[index] = newClass;
            onScheduleEdit({ ...editedSchedule, schedule });
          }}
          onCreateNextClass={() => addClass(index + 1)}
          isStartValid={isStartValid}
          isEndValid={isEndValid}
          isAutoFocused={index === recentClass}
          canBeDeleted={editedSchedule.schedule.length > 1}
          onDelete={() => {
            const schedule = [...editedSchedule.schedule];
            schedule.splice(index, 1);
            onScheduleEdit({ ...editedSchedule, schedule });
          }}
        />,
      ];
    })
    .flat();

  let error: React.ReactNode | undefined;
  if (editedSchedule.newName.trim() === ``) {
    error = <>Название не&nbsp;может быть пустым</>;
  } else if (occupiedNames.includes(editedSchedule.newName.trim())) {
    error = `Название уже использовано`;
  } else if (!isScheduleValid(editedSchedule.schedule)) {
    error = <>Некоторые звонки не&nbsp;идут друг за&nbsp;другом</>;
  }

  const buttons = (startClosing: () => void) => (
    <>
      {error && <span className="ringScheduleEditor_error">{error}</span>}
      {canBeDeleted && (
        <Button
          appearance="light"
          className="ringScheduleEditor_delete"
          onClick={() => {
            onRemove();
            startClosing();
          }}
          label="Удалить"
        />
      )}
      <Button
        className="ringScheduleEditor_save"
        isDisabled={error !== undefined}
        onClick={() => {
          onSave();
          startClosing();
        }}
        label="Сохранить"
      />
    </>
  );

  const handleBlur = useCallback(
    (event: React.FocusEvent) => {
      if (event.currentTarget.contains(event.relatedTarget as Node)) {
        return;
      }

      if (editedSchedule.schedule.length === 1) {
        return;
      }

      const schedule = editedSchedule.schedule.filter(
        ({ start, end }) =>
          start.hour !== undefined ||
          start.minute !== undefined ||
          end.hour !== undefined ||
          end.minute !== undefined,
      );

      if (editedSchedule.schedule.length > schedule.length) {
        onScheduleEdit({
          ...editedSchedule,
          schedule,
        });
      }
    },
    [editedSchedule, onScheduleEdit],
  );

  return (
    <Dialog onClose={() => onScheduleEdit(null)} buttons={buttons}>
      {startClosing => (
        <>
          <header className="ringScheduleEditor_header">
            <TextField
              label="Название"
              id="ringSchedule_name"
              className="ringScheduleEditor_name"
              value={editedSchedule.newName}
              onInput={newName =>
                onScheduleEdit({ ...editedSchedule, newName })
              }
              shouldAutoFocus={true}
            />
            <CloseButton onClick={startClosing} />
          </header>
          <ul className="ringScheduleEditor_rings" onBlur={handleBlur}>
            {classes}
            <AddClass
              onClick={() => addClass(editedSchedule.schedule.length)}
            />
          </ul>
        </>
      )}
    </Dialog>
  );
};

export default ScheduleEditor;
