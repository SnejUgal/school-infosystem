import React, { useState } from "react";
import NumericField from "../NumericField";
import { IntermediateClassRings } from "./index";
import { Ring as IRing } from "@infosystem/types/lib/ringSchedule";
import { ReactComponent as DeleteIcon } from "./icons/delete.svg";

interface TimePartProps {
  ringType: `start` | `end`;
  type: `hour` | `minute`;
  index: number;
  value?: number;
  isValid: boolean;
  isAutoFocused?: boolean;
  onChange(value: number): void;
  onCreateNextClass?(): void;
}

const TimePart = ({
  ringType,
  type,
  index,
  value,
  onChange,
  isValid,
  isAutoFocused,
  onCreateNextClass,
}: TimePartProps) => {
  const [isInRange, setIsInRange] = useState(true);

  return (
    <NumericField
      className="ringScheduleEditor_timePart"
      label={type === `hour` ? `Час` : `Минута`}
      id={`ringSchedule_${ringType}${type}-${index}`}
      value={value ?? null}
      onInput={onChange}
      shouldAutoFocus={isAutoFocused}
      onFocusNext={() => {
        let nextId;
        if (ringType === `start` && type === `hour`) {
          nextId = `ringSchedule_startminute-${index}`;
        } else if (ringType === `start` && type === `minute`) {
          nextId = `ringSchedule_endhour-${index}`;
        } else if (ringType === `end` && type === `hour`) {
          nextId = `ringSchedule_endminute-${index}`;
        } else {
          /* ringType === `end` && type === `minute` */
          nextId = `ringSchedule_starthour-${index + 1}`;
        }

        const nextField = document.querySelector<HTMLInputElement>(
          `#${nextId}`,
        );

        if (nextField) {
          nextField.focus();
          nextField.select();
        } else {
          onCreateNextClass?.();
        }
      }}
      hasError={!isValid || !isInRange}
      onValidityCheck={setIsInRange}
      min={0}
      max={type === `hour` ? 23 : 59}
    />
  );
};

interface RingProps {
  type: `start` | `end`;
  index: number;
  ring: Partial<IRing>;
  isValid: boolean;
  onChange(ring: Partial<IRing>): void;
  onCreateNextClass?(): void;
  isAutoFocused?: boolean;
}

const Ring = ({
  type,
  index,
  ring,
  isValid,
  onChange,
  onCreateNextClass,
  isAutoFocused,
}: RingProps) => {
  return (
    <>
      <TimePart
        ringType={type}
        type="hour"
        index={index}
        value={ring.hour}
        isValid={isValid}
        onChange={hour => onChange({ ...ring, hour })}
        isAutoFocused={isAutoFocused}
      />
      <span className="ringScheduleEditor_separator">:</span>
      <TimePart
        ringType={type}
        type="minute"
        index={index}
        value={ring.minute}
        isValid={isValid}
        onChange={minute => onChange({ ...ring, minute })}
        onCreateNextClass={onCreateNextClass}
      />
    </>
  );
};

interface Props {
  index: number;
  class: IntermediateClassRings;
  onChange(ring: IntermediateClassRings): void;
  onCreateNextClass(): void;
  onDelete(): void;
  isStartValid: boolean;
  isEndValid: boolean;
  isAutoFocused: boolean;
  canBeDeleted: boolean;
}

const Class = ({
  index,
  class: currentClass,
  onChange,
  isStartValid,
  isEndValid,
  onCreateNextClass,
  isAutoFocused,
  canBeDeleted,
  onDelete,
}: Props) => {
  return (
    <li className="ringScheduleEditor_class">
      <Ring
        type="start"
        index={index}
        ring={currentClass.start}
        onChange={start => onChange({ ...currentClass, start })}
        isValid={isStartValid}
        isAutoFocused={isAutoFocused}
      />
      <span className="ringScheduleEditor_separator">—</span>
      <Ring
        type="end"
        index={index}
        ring={currentClass.end}
        onChange={end => onChange({ ...currentClass, end })}
        isValid={isEndValid}
        onCreateNextClass={onCreateNextClass}
      />
      {canBeDeleted && (
        <button className="ringScheduleEditor_deleteClass" onClick={onDelete}>
          <DeleteIcon />
        </button>
      )}
    </li>
  );
};

export default Class;
