import React from "react";
import Button from "../Button";
import { ReactComponent as EditIcon } from "./icons/edit.svg";
import { RingSchedule } from "@infosystem/types/lib/ringSchedule";

interface Props {
  schedule: RingSchedule;
  isActive: boolean;
  onActivation(): void;
  onEdit(): void;
}

const ScheduleCard = ({ schedule, isActive, onActivation, onEdit }: Props) => {
  return (
    <li className="ringSchedule">
      <div className="ringSchedule_header">
        <h2 className="ringSchedule_name">{schedule.name}</h2>
        <Button
          className="ringSchedule_activate"
          isDisabled={isActive}
          onClick={onActivation}
          label={isActive ? `Активно` : `Активировать`}
        />
      </div>
      <ol className="ringSchedule_schedule">
        {schedule.schedule.map((classRings, index) => {
          const startHour = String(classRings.start.hour).padStart(2, `0`);
          const startMinute = String(classRings.start.minute).padStart(2, `0`);
          const start = `${startHour}:${startMinute}`;
          const endHour = String(classRings.end.hour).padStart(2, `0`);
          const endMinute = String(classRings.end.minute).padStart(2, `0`);
          const end = `${endHour}:${endMinute}`;
          return (
            <li key={index} className="ringSchedule_class">
              {start} — {end}
            </li>
          );
        })}
      </ol>
      <Button
        className="ringSchedule_edit"
        appearance="light"
        icon={EditIcon}
        onClick={onEdit}
        label="Редактировать"
      />
    </li>
  );
};

export default ScheduleCard;
