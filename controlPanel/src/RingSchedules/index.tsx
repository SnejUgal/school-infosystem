import "./style.scss";

import React, { useState } from "react";
import Header from "../Header";
import Button from "../Button";
import { ReactComponent as NewIcon } from "./icons/new.svg";
import {
  RingSchedule,
  Ring,
  ClassRings,
} from "@infosystem/types/lib/ringSchedule";
import { useSubscription, useQuery, useMutation } from "@apollo/react-hooks";
import ScheduleCard from "./ScheduleCard";
import ScheduleEditor from "./ScheduleEditor";
import { loader } from "graphql.macro";

export interface IntermediateClassRings {
  start: Partial<Ring>;
  end: Partial<Ring>;
  id: number;
}

export interface EditedSchedule {
  oldName?: string;
  newName: string;
  schedule: IntermediateClassRings[];
}

const INITIAL_QUERY = loader("./queries/initialQuery.graphql");
const SCHEDULES_SUBSCRIPTION = loader(
  "./queries/schedulesSubscription.graphql",
);
const ACTIVE_SCHEDULE_SUBSCRIPTION = loader(
  "./queries/activeScheduleSubscription.graphql",
);
interface Result {
  schedules: RingSchedule[];
  activeSchedule: { name: string };
}

const ACTIVATE = loader("./queries/activate.graphql");
interface ActivateVariables {
  name: string;
}

const CREATE = loader("./queries/create.graphql");
interface CreateVariables {
  name: string;
  schedule: ClassRings[];
}

const EDIT = loader("./queries/edit.graphql");
interface EditVariables {
  oldName: string;
  newName: string;
  schedule: ClassRings[];
}

const REMOVE = loader("./queries/remove.graphql");
interface RemoveVariables {
  name: string;
}

interface Props {
  container?: HTMLElement;
}

const RingSchedules = ({ container }: Props) => {
  const [schedules, setSchedules] = useState<RingSchedule[]>([]);
  const [activeSchedule, setActiveShedule] = useState(``);

  useQuery<Result>(INITIAL_QUERY, {
    onCompleted({ schedules, activeSchedule }) {
      setSchedules(schedules);
      setActiveShedule(activeSchedule.name);
    },
  });

  useSubscription<Pick<Result, "schedules">>(SCHEDULES_SUBSCRIPTION, {
    onSubscriptionData({ subscriptionData: { data } }) {
      if (data?.schedules) {
        setSchedules(data.schedules);
      }
    },
  });

  useSubscription<Pick<Result, "activeSchedule">>(
    ACTIVE_SCHEDULE_SUBSCRIPTION,
    {
      onSubscriptionData({ subscriptionData: { data } }) {
        if (data?.activeSchedule) {
          setActiveShedule(data.activeSchedule.name);
        }
      },
    },
  );

  const [activate] = useMutation<{}, ActivateVariables>(ACTIVATE);
  const [create] = useMutation<{}, CreateVariables>(CREATE);
  const [edit] = useMutation<{}, EditVariables>(EDIT);
  const [remove] = useMutation<{}, RemoveVariables>(REMOVE);

  const [editedSchedule, setEditedSchedule] = useState<EditedSchedule | null>(
    null,
  );

  return (
    <>
      <Header
        title="Расписание звонков"
        buttons={
          <Button
            icon={NewIcon}
            onClick={() =>
              setEditedSchedule({
                newName: ``,
                schedule: [{ start: {}, end: {}, id: 0 }],
              })
            }
            label="Создать новое"
          />
        }
        container={container}
      />
      <ul className="rings">
        {schedules.map(schedule => (
          <ScheduleCard
            key={schedule.name}
            schedule={schedule}
            isActive={schedule.name === activeSchedule}
            onActivation={() =>
              activate({ variables: { name: schedule.name } })
            }
            onEdit={() =>
              setEditedSchedule({
                oldName: schedule.name,
                newName: schedule.name,
                schedule: schedule.schedule.map((classRings, id) => ({
                  ...classRings,
                  id,
                })),
              })
            }
          />
        ))}
      </ul>
      {editedSchedule && (
        <ScheduleEditor
          editedSchedule={editedSchedule}
          onScheduleEdit={setEditedSchedule}
          canBeDeleted={
            editedSchedule.oldName !== undefined &&
            editedSchedule.oldName !== activeSchedule
          }
          occupiedNames={schedules
            .map(({ name }) => name)
            .filter(name => name !== editedSchedule.oldName)}
          onRemove={() =>
            remove({ variables: { name: editedSchedule.oldName! } })
          }
          onSave={() => {
            const schedule = editedSchedule.schedule.map(({ start, end }) => ({
              start: start as Ring,
              end: end as Ring,
            }));
            const { oldName } = editedSchedule;
            const newName = editedSchedule.newName.trim();

            if (oldName) {
              edit({ variables: { oldName, newName, schedule } });
            } else {
              create({ variables: { name: newName, schedule } });
            }
          }}
        />
      )}
    </>
  );
};

export default RingSchedules;
