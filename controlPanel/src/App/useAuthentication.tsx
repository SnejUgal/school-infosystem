import { loader } from "graphql.macro";
import { ApolloClient } from "apollo-client";
import { useQuery } from "@apollo/react-hooks";
import { useCallback, useState } from "react";

const IS_AUTHENTICATED = loader("./queries/isAuthenticated.graphql");
interface IsAuthenticated {
  isAuthenticated: boolean;
}

const SIGN_IN = loader("./queries/signIn.graphql");
const SIGN_OUT = loader("./queries/signOut.graphql");

interface Props<C> {
  client: ApolloClient<C>;
}

interface Authentication {
  isAuthenticated: boolean | null;
  signIn(key: string): Promise<boolean>;
  signOut(): Promise<void>;
}

function useAuthentication<C>({ client }: Props<C>): Authentication {
  const [isAuthenticated, setIsAuthenticated] = useState<boolean | null>(null);

  useQuery<IsAuthenticated>(IS_AUTHENTICATED, {
    client,
    onCompleted: ({ isAuthenticated }) => setIsAuthenticated(isAuthenticated),
  });

  const signIn = useCallback(
    async (key: string) => {
      const { data } = await client.mutate<IsAuthenticated>({
        mutation: SIGN_IN,
        variables: { key },
      });

      if (data) {
        setIsAuthenticated(data.isAuthenticated);
        return data.isAuthenticated;
      }

      return false;
    },
    [client],
  );

  const signOut = useCallback(async () => {
    setIsAuthenticated(false);
    await client.mutate<IsAuthenticated>({ mutation: SIGN_OUT });
  }, [client]);

  return {
    isAuthenticated,
    signIn,
    signOut,
  };
}

export default useAuthentication;
