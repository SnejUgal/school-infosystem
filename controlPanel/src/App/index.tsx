import "./style.scss";

import React, { useState } from "react";
import { Switch, Route, BrowserRouter, Redirect } from "react-router-dom";
import Authentication from "../Authentication";
import ControlPanel from "../ControlPanel";
import ApolloClient from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { WebSocketLink } from "apollo-link-ws";
import { HttpLink } from "apollo-link-http";
import { split } from "apollo-link";
import { getMainDefinition } from "apollo-utilities";
import useAuthentication from "./useAuthentication";
import { Section } from "../types";
import { ApolloProvider } from "@apollo/react-hooks";

const httpLink = new HttpLink({ uri: `/api`, credentials: `same-origin` });

const subscriptionUrlParsed = new URL(window.location.origin);
subscriptionUrlParsed.protocol = `ws:`;
subscriptionUrlParsed.pathname = `/api`;
const subscriptionUrl = subscriptionUrlParsed.toString();

const wsLink = new WebSocketLink({
  uri: subscriptionUrl,
  options: { reconnect: true },
});

const link = split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === `OperationDefinition` &&
      definition.operation === `subscription`
    );
  },
  wsLink,
  httpLink,
);

const client = new ApolloClient({
  link,
  cache: new InMemoryCache({ addTypename: false }),
  defaultOptions: {},
});

const ACTIVE_SECTION = `activeSection`;

const App = () => {
  const { isAuthenticated, signIn, signOut } = useAuthentication({ client });
  const [section, setSection] = useState<Section>(
    () => (localStorage.getItem(ACTIVE_SECTION) as Section) ?? `rings`,
  );

  let sectionPath;

  if (isAuthenticated === null) {
    sectionPath = `/`;
  } else if (isAuthenticated) {
    sectionPath = `/${section}`;
  } else {
    sectionPath = `/authentication`;
  }

  const handleSectionChange = (section: Section) => {
    setSection(section);
    localStorage.setItem(ACTIVE_SECTION, section);
  };

  return (
    <ApolloProvider client={client}>
      <BrowserRouter basename="/controlPanel">
        <Redirect to={sectionPath} />
        <Switch>
          <Route exact path="/authentication">
            <Authentication signIn={signIn} />
          </Route>
          <Route>
            <ControlPanel
              signOut={signOut}
              section={section}
              onSectionChange={handleSectionChange}
            />
          </Route>
        </Switch>
      </BrowserRouter>
    </ApolloProvider>
  );
};

export default App;
