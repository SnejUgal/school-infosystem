import "./style.scss";

import React, { useState } from "react";
import TextField from "../TextField";
import Button from "../Button";

interface Props {
  signIn(key: string): Promise<boolean>;
}

const Authentication = ({ signIn }: Props) => {
  const [key, setKey] = useState(``);
  const [hasFailed, setHasFailed] = useState(false);
  const [isAuthenticating, setIsAuthenticating] = useState(false);

  const handleTokenInput = (token: string) => {
    setKey(token);
    setHasFailed(false);
  };

  const handleSubmit = async (event: React.FormEvent) => {
    event.preventDefault();
    setIsAuthenticating(true);

    const hasAuthenticated = await signIn(key);
    setIsAuthenticating(false);
    setHasFailed(!hasAuthenticated);
  };

  return (
    <div className="authentication">
      <header className="authentication_header">
        <img
          className="authentication_logo"
          src="/resources/logo.png"
          alt="Логотип школы"
        />
        <h1 className="authentication_title">Вход в панель управления</h1>
      </header>
      <main className="authentication_main">
        <form onSubmit={handleSubmit}>
          <TextField
            className="authentication_tokenField"
            id="authentication_token"
            label="Ключ аутентификации"
            value={key}
            onInput={handleTokenInput}
            type="password"
            shouldAutoFocus={true}
            error={hasFailed ? `Неверный ключ аутентификации` : undefined}
            isDisabled={isAuthenticating}
          />
          <Button type="submit" isDisabled={isAuthenticating} label="Войти" />
        </form>
      </main>
      <footer className="authentication_footer">
        <p className="authentication_license">
          Школьная информационая система является свободным программным
          обеспечением, лицензированным под GNU AGPL v3.0 или поздней версии.
          Исходный код всех компонентов данного ПО {}
          <a
            className="authentication_sourceCode"
            href="https://gitlab.com/SnejUgal/school-infosystem"
          >
            доступен на GitLab
          </a>
          .
        </p>
      </footer>
    </div>
  );
};

export default Authentication;
