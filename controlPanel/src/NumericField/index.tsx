import React, { useState } from "react";
import TextField, { CommonProps } from "../TextField";

interface Props extends CommonProps {
  value: number | null;
  onInput(value: number): void;
  onValidityCheck?(isValid: boolean): void;
  onFocusNext?(): void;
}

const NumericField = (props: Props) => {
  const [stringValue, setStringValue] = useState(() =>
    props.value === null ? `` : String(props.value),
  );

  const isInRange = (value: number) => {
    if (!Number.isFinite(value)) {
      return false;
    }

    const isAboveMin = props.min !== undefined ? props.min <= value : true;
    const isBelowMax = props.max !== undefined ? value <= props.max : true;
    return isAboveMin && isBelowMax;
  };

  const handleInput = (newStringValue: string) => {
    setStringValue(newStringValue);

    const parsed = Number(newStringValue.trim());

    if (isInRange(parsed)) {
      props.onInput(parsed);
    }
  };

  return (
    <TextField
      {...props}
      type="number"
      value={stringValue}
      onInput={handleInput}
      onKeyUp={event => {
        if (
          props.onFocusNext &&
          event.key.length === 1 &&
          Number.isFinite(Number.parseInt(event.key)) &&
          (event.currentTarget.valueAsNumber * 10 > (props.max ?? Infinity) ||
            stringValue.length >= Math.log10(props.max ?? Infinity))
        ) {
          props.onFocusNext();
        }
      }}
      onFocus={event => event.currentTarget.select()}
      onBlur={() => {
        props.onValidityCheck?.(isInRange(Number.parseInt(stringValue)));
      }}
    />
  );
};

export default NumericField;
