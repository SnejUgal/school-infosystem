import "./style.scss";

import React from "react";
import classNames from "classnames";

interface Props {
  type?: `button` | `submit`;
  appearance?: `bright` | `light`;
  icon?: React.JSXElementConstructor<{ className: string }>;
  label?: string;
  className?: string;
  isDisabled?: boolean;
  onClick?(): void;
}

const Button = (props: Props) => {
  const Icon = props.icon;
  const className = classNames(`button`, props.className, {
    "-isBright": props.appearance ? props.appearance === `bright` : true,
    "-isLight": props.appearance === `light`,
  });

  return (
    <button
      className={className}
      type={props.type ?? `button`}
      disabled={props.isDisabled}
      onClick={props.onClick}
    >
      {Icon && <Icon className="button_icon" />}
      {props.label && <span className="button_label">{props.label}</span>}
    </button>
  );
};

export default Button;
