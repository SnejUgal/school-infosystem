import React from "react";
import { Section } from "../types";
import classNames from "classnames";

interface ExpandPanelProps {
  type: `expandTabs`;
  onClick(): void;
}

interface SignOutProps {
  type: `signOut`;
  onClick(): void;
}

interface SectionTabProps {
  type: Section;
  isActive: boolean;
  onClick(section: Section): void;
}

interface CommonTabProps {
  label: string;
  icon: React.JSXElementConstructor<{ className: string }>;
}

type TabProps = (ExpandPanelProps | SignOutProps | SectionTabProps) &
  CommonTabProps;

const Tab = (props: TabProps) => {
  const Icon = props.icon;

  const content = (
    <>
      <Icon className="tab_icon" />
      <span className="tab_name">{props.label}</span>
    </>
  );

  if (props.type === `expandTabs` || props.type === `signOut`) {
    const handleClick = (event: React.MouseEvent) => {
      event.preventDefault();
      props.onClick();
    };

    return (
      <li className="tab">
        <button onClick={handleClick}>{content}</button>
      </li>
    );
  }

  const href = `/${props.type}`;
  const tabClassName = classNames(`tab`, {
    "-isActive": `isActive` in props ? props.isActive : false,
  });

  const handleClick = (event: React.MouseEvent) => {
    event.preventDefault();
    props.onClick(props.type);
  };

  return (
    <li className={tabClassName}>
      <a href={href} onClick={handleClick}>
        {content}
      </a>
    </li>
  );
};

export default Tab;
