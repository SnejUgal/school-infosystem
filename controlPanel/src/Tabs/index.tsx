import "./style.scss";

import React, { useState } from "react";
import { Section } from "../types";
import classNames from "classnames";

import Tab from "./Tab";
import { ReactComponent as ExpandIcon } from "./icons/expand.svg";
import { ReactComponent as RingsIcon } from "./icons/rings.svg";
import { ReactComponent as DriveEjectionIcon } from "./icons/driveEjection.svg";
import { ReactComponent as SignOutIcon } from "./icons/signOut.svg";

interface Props {
  section: Section;
  onSectionChange(section: Section): void;
  signOut(): void;
}

const IS_PANEL_EXPANDED = `isPanelExpanded`;

const Tabs = ({ section, onSectionChange, signOut }: Props) => {
  const [isExpanded, setIsExpanded] = useState(() => {
    const storageValue = localStorage.getItem(IS_PANEL_EXPANDED);

    return storageValue === `true` ? true : false;
  });

  const tabsClassName = classNames(`tabs`, { "-isExpanded": isExpanded });

  const handleExpandChange = () => {
    const newValue = !isExpanded;
    localStorage.setItem(IS_PANEL_EXPANDED, String(newValue));
    setIsExpanded(newValue);
  };

  return (
    <nav className={tabsClassName}>
      <ul className="tabs_container">
        <Tab
          type="expandTabs"
          onClick={handleExpandChange}
          icon={ExpandIcon}
          label="Закрыть меню"
        />
        <Tab
          type="rings"
          onClick={onSectionChange}
          icon={RingsIcon}
          isActive={section === `rings`}
          label="Расписание звонков"
        />
        <Tab
          type="driveEjection"
          onClick={onSectionChange}
          icon={DriveEjectionIcon}
          isActive={section === `driveEjection`}
          label="Извлечение носителей"
        />
        <Tab
          type="signOut"
          onClick={signOut}
          icon={SignOutIcon}
          label="Выйти"
        />
      </ul>
    </nav>
  );
};

export default Tabs;
