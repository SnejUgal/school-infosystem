import "./style.scss";

import React, { useState } from "react";
import classNames from "classnames";

export interface CommonProps {
  label: string;
  id: string;
  hasError?: boolean;
  error?: string;
  shouldAutoFocus?: boolean;
  className?: string;
  isDisabled?: boolean;
  min?: number;
  max?: number;
  onFocus?(event: React.FocusEvent<HTMLInputElement>): void;
  onBlur?(): void;
  onKeyUp?(event: React.KeyboardEvent<HTMLInputElement>): void;
}

interface Props extends CommonProps {
  value: string;
  type?: `text` | `password` | `number`;
  onInput(value: string): void;
}

const TextField = (props: Props) => {
  const [isFocused, setIsFocused] = useState(false);
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    props.onInput(event.target.value);
  };

  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (
      props.type === `number` &&
      event.key.length === 1 && // видимый символ
      !event.altKey &&
      !event.ctrlKey &&
      !Number.isFinite(Number.parseInt(event.key))
      // ^ цифра — пока что нам нужны только целые неотрицательные числа
    ) {
      event.preventDefault();
    }
  };

  const textFieldClassName = classNames(`textField`, props.className, {
    "-hasError": props.hasError || Boolean(props.error),
    "-isDisabled": props.isDisabled,
  });
  const labelClassName = classNames(`textField_label`, {
    "-expand":
      props.value.length === 0 && !isFocused && props.type !== `number`,
    "-isFocused": isFocused,
    "-isRightAligned": props.type === `number`,
  });

  return (
    <div className={textFieldClassName}>
      <label className={labelClassName} htmlFor={props.id}>
        {props.label}
      </label>
      <input
        className="textField_input"
        id={props.id}
        value={props.value}
        onChange={handleChange}
        autoFocus={props.shouldAutoFocus}
        type={props.type ?? `text`}
        onKeyDown={handleKeyDown}
        onKeyUp={props.onKeyUp}
        onFocus={event => {
          setIsFocused(true);
          props.onFocus?.(event);
        }}
        onBlur={() => {
          setIsFocused(false);
          props.onBlur?.();
        }}
        disabled={props.isDisabled}
        min={props.min}
        max={props.max}
      />
      {props.error && <p className="textField_error">{props.error}</p>}
    </div>
  );
};

export default TextField;
