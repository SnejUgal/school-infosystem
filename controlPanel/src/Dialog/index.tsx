import "./style.scss";

import React, { useEffect, useState, useCallback } from "react";
import { createPortal } from "react-dom";
import classNames from "classnames";
import { ReactComponent as CloseIcon } from "./icons/close.svg";

const root = document.getElementById(`dialogs`)! as HTMLDivElement;

interface CloseButtonProps {
  onClick(): void;
}

export const CloseButton = ({ onClick }: CloseButtonProps) => {
  return (
    <button className="dialog_close" onClick={onClick}>
      <CloseIcon />
    </button>
  );
};

interface Props {
  buttons?: (startClosing: () => void) => React.ReactNode;
  children: (startClosing: () => void) => React.ReactNode;
  onClose(): void;
}

const Dialog = ({ buttons, children, onClose }: Props) => {
  const [isClosing, setIsClosing] = useState(false);
  const [buttonsContainer, setButtons] = useState<HTMLDivElement | null>(null);
  const [isSticky, setIsSticky] = useState(false);

  const startClosing = useCallback(() => {
    setIsClosing(true);
  }, []);

  useEffect(() => {
    root.classList.toggle(`-showBackground`, !isClosing);
  }, [isClosing]);

  useEffect(() => {
    if (!isClosing) {
      return;
    }

    root.addEventListener(`transitionend`, onClose);

    return () => {
      root.removeEventListener(`transitionend`, onClose);
    };
  }, [isClosing, onClose]);

  useEffect(() => {
    if (!buttonsContainer || !(`IntersectionObserver` in window)) {
      return;
    }

    const observer = new IntersectionObserver(
      ([record]) => {
        // в стилях у контейнера кнопок есть лишний отступ в один пиксель,
        // который показывается на экране только тогда, когда диалог выше
        // нижней границы экрана на один пиксель. Когда контейнер прилип к низу
        // экрана, то этот лишний отступ не показывается и элемент
        // не показывается полностью => intersectionRatio < 1.
        setIsSticky(record.intersectionRatio < 1);
      },
      { threshold: [1] },
    );

    observer.observe(buttonsContainer);

    return () => observer.disconnect();
  }, [buttonsContainer]);

  const buttonsClassName = classNames(`dialog_buttons`, {
    "-isSticky": isSticky,
  });

  useEffect(() => {
    let hasStartedOnBackgdrop = false;

    const mouseDownHandler = (event: MouseEvent) => {
      if (event.target === event.currentTarget) {
        hasStartedOnBackgdrop = true;
      }
    };
    const clickHandler = (event: MouseEvent) => {
      if (hasStartedOnBackgdrop && event.target === event.currentTarget) {
        startClosing();
      }
    };

    root.addEventListener(`mousedown`, mouseDownHandler);
    root.addEventListener(`click`, clickHandler);

    return () => {
      root.removeEventListener(`mousedown`, mouseDownHandler);
      root.removeEventListener(`click`, clickHandler);
    };
  }, [startClosing]);

  const dialogClassName = classNames(`dialog`, {
    "-isClosing": isClosing,
  });

  return createPortal(
    <div className={dialogClassName}>
      <div className="dialog_content">{children(startClosing)}</div>
      {buttons && (
        <div className={buttonsClassName} ref={setButtons}>
          {buttons(startClosing)}
        </div>
      )}
    </div>,
    root,
  );
};

export default Dialog;
