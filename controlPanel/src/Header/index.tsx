import "./style.scss";

import React, { useRef, useState, useCallback, useEffect } from "react";
import classNames from "classnames";

interface Props {
  title: string;
  buttons?: React.ReactNode;
  container?: HTMLElement;
}

const Header = ({ title, buttons, container }: Props) => {
  const headerRef = useRef<HTMLElement>(null);
  const [isFixed, setIsFixed] = useState(false);

  const checkIfFixed = useCallback(() => {
    if (!headerRef.current) {
      return;
    }

    const { top } = headerRef.current.getBoundingClientRect();

    setIsFixed(top === 0);
  }, []);

  useEffect(() => {
    if (!container) {
      return;
    }

    container.addEventListener(`scroll`, checkIfFixed);

    return () => {
      container!.removeEventListener(`scroll`, checkIfFixed);
    };
  }, [container, checkIfFixed]);

  const headerClassName = classNames(`header`, {
    "-isFixed": isFixed,
  });

  return (
    <header className={headerClassName} ref={headerRef}>
      <h1 className="header_title">{title}</h1>
      <div className="header_buttons">{buttons}</div>
    </header>
  );
};

export default Header;
