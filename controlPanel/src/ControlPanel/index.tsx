import "./style.scss";

import React, { useRef } from "react";
import { Switch, Route } from "react-router-dom";
import RingSchedules from "../RingSchedules";
import Tabs from "../Tabs";
import { Section } from "../types";
import DriveEjection from "../DriveEjection";

interface Props {
  section: Section;
  onSectionChange(section: Section): void;
  signOut(): void;
}

const ControlPanel = ({ section, onSectionChange, signOut }: Props) => {
  const containerRef = useRef<HTMLDivElement>(null);

  return (
    <div className="controlPanel">
      <Tabs
        section={section}
        onSectionChange={onSectionChange}
        signOut={signOut}
      />
      <div className="controlPanel_content" ref={containerRef}>
        <Switch>
          <Route path="/rings">
            <RingSchedules container={containerRef.current ?? undefined} />
          </Route>
          <Route path="/driveEjection">
            <DriveEjection container={containerRef.current ?? undefined} />
          </Route>
        </Switch>
      </div>
    </div>
  );
};

export default ControlPanel;
