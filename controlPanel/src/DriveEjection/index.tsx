import "./style.scss";

import React, { useState } from "react";
import Header from "../Header";
import Button from "../Button";
import { useQuery, useSubscription, useMutation } from "@apollo/react-hooks";
import { loader } from "graphql.macro";
import { ReactComponent as EjectIcon } from "./icons/eject.svg";
import { ReactComponent as DriveIcon } from "./icons/drive.svg";
import CircularProgress from "@material-ui/core/CircularProgress";

const GET_DRIVES = loader("./queries/getDrives.graphql");
const WATCH_DRIVES = loader("./queries/watchDrives.graphql");

interface GetResult {
  drives: string[];
}

const UNMOUNT_DRIVE = loader("./queries/unmountDrive.graphql");

interface UnmountVariables {
  drive: string;
}

interface UnmountResult {
  isUnmounted: boolean;
}

interface ProgressProps {
  className: string;
}

const Progress = (id: string) => {
  const Progress = ({ className }: ProgressProps) => (
    <CircularProgress id={id} className={className} size={24} />
  );

  return Progress;
};

interface DriveProps {
  drive: string;
}

const Drive = ({ drive }: DriveProps) => {
  const [unmountDrive, { loading }] = useMutation<
    UnmountResult,
    UnmountVariables
  >(UNMOUNT_DRIVE, { errorPolicy: `ignore` });

  return (
    <li className="drive">
      <DriveIcon className="drive_icon" />
      <h2 className="drive_name">{drive}</h2>
      <Button
        icon={loading ? Progress(drive) : EjectIcon}
        appearance={loading ? `light` : `bright`}
        label={loading ? `` : `Извлечь`}
        isDisabled={loading}
        className="drive_eject"
        onClick={() => unmountDrive({ variables: { drive } })}
      />
    </li>
  );
};

interface DriveEjectionProps {
  container?: HTMLElement;
}

const DriveEjection = ({ container }: DriveEjectionProps) => {
  const [drives, setDrives] = useState<string[]>([]);

  useQuery<GetResult>(GET_DRIVES, {
    onCompleted: ({ drives }) => setDrives(drives),
  });

  useSubscription<GetResult>(WATCH_DRIVES, {
    onSubscriptionData: ({ subscriptionData: { data } }) => {
      if (data) {
        setDrives(data.drives);
      }
    },
  });

  return (
    <>
      <Header title="Извлечение носителей" container={container} />
      <div className="driveEjection_container">
        <p className="driveEjection_help">
          Перед&nbsp;отсоединением носителя его&nbsp;необходимо размонтировать,
          чтобы&nbsp;на&nbsp;нём не&nbsp;повредились данные. Потом&nbsp;носитель
          можно безопасно отсоединять.
        </p>
        {drives.length > 0 && (
          <ul className="driveEjection_drives">
            {drives.map(drive => (
              <Drive key={drive} drive={drive} />
            ))}
          </ul>
        )}
        {drives.length === 0 && (
          <p className="driveEjection_placeholder">
            Нет подключённых носителей
          </p>
        )}
      </div>
    </>
  );
};

export default DriveEjection;
