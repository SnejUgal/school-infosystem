import {
  isConfigAllReplacedClasses,
  ReplacedClasses,
  isConfigClassSchedule,
  ClassSchedule,
} from "@infosystem/types/lib/config";
import * as toml from "@iarna/toml";
import { promises as fs } from "fs";
import { join, dirname } from "path";
import { watch } from "chokidar";
import events, {
  CLASS_SCHEDULE_UPDATE,
  REPLACED_CLASSES_UPDATE,
} from "./events";

const CONFIGURATION_PATH = join(__dirname, `../../config.toml`);

// todo: хранение состояния в бд
export let classSchedule: ClassSchedule[] = [];
export let replacedClasses: ReplacedClasses[] = [];

const updateFromFile = async () => {
  const contents = await fs.readFile(CONFIGURATION_PATH, `utf8`);
  const parsed = toml.parse(contents);

  if (isConfigClassSchedule(parsed.classSchedule)) {
    classSchedule = Object.entries(parsed.classSchedule).map(
      ([grade, schedule]) => ({
        grade,
        schedule,
      }),
    );

    events.publish(CLASS_SCHEDULE_UPDATE, classSchedule);
  } else if (parsed.classSchedule === undefined) {
    classSchedule = [];
    events.publish(CLASS_SCHEDULE_UPDATE, classSchedule);
  }

  if (isConfigAllReplacedClasses(parsed.replacedClasses)) {
    replacedClasses = Object.entries(parsed.replacedClasses).map(
      ([date, grades]) => ({
        date,
        grades: Object.entries(grades).map(([grade, classes]) => ({
          grade,
          classes: Object.entries(classes).map(([index, replacingClass]) => ({
            index: Number(index),
            class: replacingClass,
          })),
        })),
      }),
    );

    events.publish(REPLACED_CLASSES_UPDATE, replacedClasses);
  } else if (parsed.replacedClasses === undefined) {
    replacedClasses = [];
    events.publish(REPLACED_CLASSES_UPDATE, replacedClasses);
  }
};

// Просто отслеживать configPath не получится, так как некоторые редакторы,
// такие как gedit, при сохранении заменяют файл, а не перезаписывают его,
// в результате чего изменяется inode. А файл отслеживается по его inode,
// а не по его пути. Поэтому сервер в таком случае не улавливал бы изменения
// конфига.
watch(dirname(CONFIGURATION_PATH), { depth: 0 }).on(`all`, async (_, path) => {
  if (path === CONFIGURATION_PATH) {
    await updateFromFile();
  }
});
