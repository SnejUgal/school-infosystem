import { Pool } from "pg";
import {
  ClassRings,
  Ring,
  RingSchedule,
} from "@infosystem/types/lib/ringSchedule";
import events, {
  RING_SCHEDULES_UPDATE,
  ACTIVE_RING_SCHEDULE_UPDATE,
} from "./events";

const parseRing = (ring: string): Ring => {
  const [hour, minute] = ring.split(`:`).map((x) => Number.parseInt(x, 10));

  return { hour, minute };
};

const serializeRing = (ring: Ring): string => {
  const hour = String(ring.hour).padStart(2, `0`);
  const minute = String(ring.minute).padStart(2, `0`);

  return `${hour}:${minute}:00`;
};

const parseClassRings = (classRings: string): ClassRings => {
  const [start, end] = classRings.slice(1, -1).split(`,`);

  return {
    start: parseRing(start),
    end: parseRing(end),
  };
};

const serializeClassRings = (ring: ClassRings): string => {
  const start = serializeRing(ring.start);
  const end = serializeRing(ring.end);

  return `[${start},${end})`;
};

const emitRingSchedulesUpdate = async () => {
  const schedules = await getAllRingSchedules();
  events.publish(RING_SCHEDULES_UPDATE, schedules);
};

const pool = new Pool();

export const getAllRingSchedules = async (): Promise<RingSchedule[]> => {
  interface Row {
    name: string;
    schedule: string[];
  }

  const { rows } = await pool.query<Row>(
    `select name, schedule::text[]
      from "ringSchedules";`,
  );

  return rows.map(({ name, schedule }) => ({
    name,
    schedule: schedule.map(parseClassRings),
  }));
};

export const getRingSchedule = async (name: string): Promise<RingSchedule> => {
  interface Row {
    schedule: string[];
  }

  const { rows } = await pool.query<Row>(
    `select schedule::text[]
      from "ringSchedules"
      where name = $1
      limit 1;`,
    [name],
  );

  if (rows.length === 0) {
    throw new Error(`No ring schedule named "${name}" exists`);
  }

  return {
    name,
    schedule: rows[0].schedule.map(parseClassRings),
  };
};

export const createRingSchedule = async ({
  name,
  schedule,
}: RingSchedule): Promise<RingSchedule> => {
  const serializedRingSchedule = schedule.map(serializeClassRings);

  try {
    await pool.query(
      `insert
      into "ringSchedules" (name, schedule)
      values ($1, $2)`,
      [name, serializedRingSchedule],
    );
  } catch {
    throw new Error(`A ring schedule named "${name}" already exists`);
  }

  emitRingSchedulesUpdate();

  return { name, schedule };
};

export const editRingSchedule = async (
  oldName: string,
  newSchedule: Partial<RingSchedule>,
): Promise<RingSchedule> => {
  const oldSchedule = await getRingSchedule(oldName);
  newSchedule.name = newSchedule.name ?? oldSchedule.name;
  newSchedule.schedule = newSchedule.schedule ?? oldSchedule.schedule;

  const isRenamed = oldName !== newSchedule.name;
  if (isRenamed) {
    const { rows } = await pool.query(
      `select true as "alreadyExists"
        from "ringSchedules"
        where name = $1
        limit 1;`,
      [newSchedule.name],
    );

    if (rows.length > 0) {
      throw new Error(
        `A ring schedule named "${newSchedule.name}" already exists`,
      );
    }
  }

  const serializedRingSchedule = newSchedule.schedule.map(serializeClassRings);

  const queries = [
    pool.query(
      `update "ringSchedules"
        set "name" = $2, schedule = $3
        where name = $1;`,
      [oldName, newSchedule.name, serializedRingSchedule],
    ),
  ];

  if (isRenamed) {
    queries.push(
      pool.query(
        `update state
          set "activeRingSchedule" = $2
          where "activeRingSchedule" = $1`,
        [oldName, newSchedule.name],
      ),
    );
  }

  await Promise.all(queries);

  emitRingSchedulesUpdate();

  if (newSchedule.name === (await getActiveRingScheduleName())) {
    events.publish(ACTIVE_RING_SCHEDULE_UPDATE, newSchedule as RingSchedule);
  }

  return newSchedule as RingSchedule;
};

export const deleteRingSchedule = async (
  name: string,
): Promise<RingSchedule> => {
  const activeRingScheduleName = await getActiveRingScheduleName();

  if (name === activeRingScheduleName) {
    throw new Error(`Cannot remove the active ring schedule`);
  }

  interface Row {
    name: string;
    schedule: string[];
  }

  const {
    rows: [deletedSchedule],
  } = await pool.query<Row>(
    `delete
      from "ringSchedules"
      where name = $1
      returning name, schedule::text[];`,
    [name],
  );

  emitRingSchedulesUpdate();

  return {
    name: deletedSchedule.name,
    schedule: deletedSchedule.schedule.map(parseClassRings),
  };
};

export const setActiveRingSchedule = async (
  name: string,
): Promise<RingSchedule> => {
  interface Row {
    name: string;
  }

  const { rows } = await pool.query<Row>(
    `select name
      from "ringSchedules";`,
  );

  const schedules = rows.map((x) => x.name);

  if (!schedules.includes(name)) {
    throw new Error(`No ring schedule with name "${name}" exists`);
  }

  await pool.query(
    `update state
      set "activeRingSchedule" = $1;`,
    [name],
  );

  const newSchedule = await getActiveRingSchedule();
  events.publish(ACTIVE_RING_SCHEDULE_UPDATE, newSchedule);

  return newSchedule;
};

export const getActiveRingSchedule = async (): Promise<RingSchedule> => {
  interface Row {
    name: string;
    schedule: string[];
  }

  const {
    rows: [{ name, schedule }],
  } = await pool.query<Row>(
    `select name, schedule::text[]
      from "ringSchedules", state
      where "activeRingSchedule" = name
      limit 1;`,
  );

  return {
    name,
    schedule: schedule.map(parseClassRings),
  };
};

export const getActiveRingScheduleName = async (): Promise<string> => {
  interface Row {
    activeRingSchedule: string;
  }

  const {
    rows: [{ activeRingSchedule }],
  } = await pool.query<Row>(
    `select "activeRingSchedule"
      from state
      limit 1;`,
  );

  return activeRingSchedule;
};

export const addNewToken = async (key: string, token: string) => {
  await pool.query(
    `insert
      into "authenticationTokens" (key, token)
      values ($1, $2);`,
    [key, token],
  );
};

export const invalidateToken = async (token: string) => {
  await pool.query(
    `delete
      from "authenticationTokens"
      where token = $1`,
    [token],
  );
};

export const isTokenValid = async (token: string): Promise<boolean> => {
  const { rows } = await pool.query(
    `select true as "isTokenValid"
      from "authenticationTokens"
      where token = $1
      limit 1;`,
    [token],
  );

  return rows.length > 0;
};

export const isKeyValid = async (key: string): Promise<boolean> => {
  const { rows } = await pool.query(
    `select true as "isKeyValid"
      from "authenticationKeys"
      where key = $1
      limit 1;`,
    [key],
  );

  return rows.length > 0;
};
