import { Video } from "@infosystem/types/lib/videos";
import * as path from "path";
import { watch, promises as fs, ReadStream, createReadStream } from "fs";
import { userInfo } from "os";
import { spawn } from "child_process";
import chokidar from "chokidar";
import { debounce } from "lodash";
import events, { DRIVES_UPDATE, VIDEOS_UPDATE } from "./events";

interface RawVideo {
  order: number;
  path: string;
  subtitlesPath?: string;
}

const DRIVE_MOUNT_DIRECTORY = path.join(`/media`, userInfo().username);
const VIDEO_DIRECTORY_NAME = `Информационный экран`;
const UNMOUNT_TIMEOUT = 60 * 1000;

const SUBTITLES_EXTENSIONS = [`.vtt`];
// Форматы, которые поддерживает Chromium
// https://chromium.googlesource.com/chromium/src/+/master/net/base/mime_util.cc#576
const VIDEO_EXTENSIONS = [`.mp4`, `.webm`, `.mkv`, `.ogg`, `.ogv`];

const unmount = (drive: string) =>
  new Promise<void>((resolve, reject) => {
    const drivePath = path.join(DRIVE_MOUNT_DIRECTORY, drive);
    const umount = spawn(`umount`, [`--lazy`, drivePath]);

    umount.on(`close`, async (code) => {
      if (code === 0) {
        resolve();
      } else {
        try {
          await fs.access(drivePath);
          reject();
        } catch {
          resolve();
        }
      }
    });
  });

const videos: Map<number, RawVideo> = new Map();
const subtitles: Map<string, Set<string>> = new Map();
let idCounter = 0;
// сами ведём список подключенных носителей, потому что он не всегда отражает
// /media/$USER — например, когда носитель извлекается через информационный
// экран
const drives: Set<string> = new Set();
const streams: Map<string, Set<ReadStream>> = new Map();

const handleFileCreation = (filePath: string) => {
  const name = path.basename(filePath);
  const order = Number.parseFloat(name);

  if (!Number.isFinite(order)) {
    return;
  }

  const extension = path.extname(name);
  const directory = path.dirname(filePath);

  if (VIDEO_EXTENSIONS.includes(extension)) {
    const subtitlesPath = [...(subtitles.get(directory) ?? [])].find(
      (path) => Number.parseFloat(path) === order,
    );

    videos.set(idCounter, {
      order,
      path: filePath,
      subtitlesPath,
    });
    idCounter++;
    handleVideosUpdate();
  } else if (SUBTITLES_EXTENSIONS.includes(extension)) {
    const exisitingSubtitles = subtitles.get(directory);

    if (exisitingSubtitles) {
      exisitingSubtitles.add(filePath);
    } else {
      subtitles.set(path.dirname(filePath), new Set([filePath]));
    }

    for (const video of videos.values()) {
      if (video.order === order && path.dirname(video.path) === directory) {
        video.subtitlesPath = filePath;
        handleVideosUpdate();
      }
    }
  }
};

const handleFileRemoval = (filePath: string) => {
  const name = path.basename(filePath);
  const extension = path.extname(filePath);
  const directory = path.dirname(filePath);

  if (VIDEO_EXTENSIONS.includes(extension)) {
    for (const [id, { path }] of videos) {
      if (path === filePath) {
        videos.delete(id);
        handleVideosUpdate();
      }
    }
  } else if (SUBTITLES_EXTENSIONS.includes(extension)) {
    const subtitlesInDirectory = subtitles.get(directory);

    if (!subtitlesInDirectory) {
      return;
    }

    subtitlesInDirectory.delete(filePath);

    if (subtitlesInDirectory.size === 0) {
      subtitles.delete(directory);
    }

    const order = Number.parseFloat(name);

    if (!Number.isFinite(order)) {
      return;
    }

    for (const video of videos.values()) {
      if (video.order === order && path.dirname(video.path) === directory) {
        video.subtitlesPath = undefined;
        handleVideosUpdate();
      }
    }
  }
};

const videoWatcher = chokidar
  .watch([], {
    ignored: (x: string) => x.split(path.sep).some((x) => x.startsWith(`.`)),
    disableGlobbing: true,
  })
  .on(`add`, handleFileCreation)
  .on(`change`, (path) => {
    handleFileRemoval(path);
    handleFileCreation(path);
  })
  .on(`unlink`, handleFileRemoval);

const driveWatcher = chokidar
  .watch([], { depth: 0, disableGlobbing: true })
  .on(`addDir`, (directory) => {
    if (path.basename(directory) === VIDEO_DIRECTORY_NAME) {
      videoWatcher.add(directory);
    }
  })
  .on(`unlinkDir`, async (directory) => {
    if (path.basename(directory) === VIDEO_DIRECTORY_NAME) {
      await videoWatcher.unwatch(directory);
      await removeVideos(directory);
    }
  });

const emitDrivesUpdate = debounce(
  (drives: string[]) => events.publish(DRIVES_UPDATE, drives),
  50,
);
const handleDrivesUpdate = () => emitDrivesUpdate([...drives]);

const emitVideosUpdate = debounce(
  (videos: Video[]) => events.publish(VIDEOS_UPDATE, videos),
  50,
);
const handleVideosUpdate = () => emitVideosUpdate(getList());

const removeVideos = async (drive: string) => {
  const drivePath = path.join(DRIVE_MOUNT_DIRECTORY, drive);

  videoWatcher.unwatch(drivePath);

  for (const [id, { path }] of videos) {
    if (path.startsWith(drivePath)) {
      videos.delete(id);
    }
  }

  for (const path of subtitles.keys()) {
    if (path.startsWith(drivePath)) {
      subtitles.delete(path);
    }
  }

  for (const [path, streamsOnDrive] of streams) {
    if (path.startsWith(drivePath)) {
      for (const stream of streamsOnDrive) {
        stream.destroy();
      }

      streams.delete(path);
    }
  }

  handleVideosUpdate();
};

export const unmountDrive = async (drive: string): Promise<void> => {
  if (!drives.has(drive)) {
    throw new Error(`Found no drive named ${drive}`);
  }

  driveWatcher.unwatch(path.join(DRIVE_MOUNT_DIRECTORY, drive));
  await removeVideos(drive);

  const start = Date.now();

  while (true) {
    try {
      await unmount(drive);
      const isDeleted = drives.delete(drive);
      if (isDeleted) {
        handleDrivesUpdate();
      }

      return;
    } catch {
      if (Date.now() - start > UNMOUNT_TIMEOUT) {
        throw new Error(`Failed to unmount ${drive}`);
      }
    }
  }
};

export const getList = (): Video[] => {
  // Map нужен для того, чтобы в выходном списке не было элементов с одним
  // и тем же порядковым номером
  const list = new Map(
    [...videos].map(([id, { order, subtitlesPath }]) => {
      const hasSubtitles = Boolean(subtitlesPath);
      return [order, { order, id, hasSubtitles }];
    }),
  );

  return [...list.values()];
};

export const getDrives = () => [...drives];

const getStream = (path: string) => {
  const stream = createReadStream(path);

  if (!streams.has(path)) {
    streams.set(path, new Set());
  }
  streams.get(path)!.add(stream);

  return stream;
};

export const getVideoStream = (id: number) => {
  const path = videos.get(id)?.path;

  if (!path) {
    return null;
  }

  return getStream(path);
};

export const getSubtitleStream = (id: number) => {
  const path = videos.get(id)?.subtitlesPath;

  if (!path) {
    return null;
  }

  return getStream(path);
};

const handleDriveMount = async (drive: string) => {
  // Флешку либо вставили, либо вытащили
  const fullPath = path.join(DRIVE_MOUNT_DIRECTORY, drive);

  while (true) {
    try {
      await fs.access(fullPath);

      // Флешка примонтирована — ищем новые видео
      drives.add(drive);
      handleDrivesUpdate();

      while (true) {
        // пробуем, пока не будет полного доступа к флешке
        try {
          watch(fullPath).close();
          break;
        } catch {}
      }
      driveWatcher.add(fullPath);

      break;
    } catch (error: any) {
      if (error.code === `ENOENT`) {
        // Флешку вытащили — удаляем видео, которые были на ней
        const isDeleted = drives.delete(drive);
        if (isDeleted) {
          removeVideos(drive);
          driveWatcher.unwatch(fullPath);
          handleDrivesUpdate();
        }
        break;
      }

      // Флешку вставили, но рабочее окружение ещё не примонтировало её
      // и в доступе отказано — пробуем ещё раз
      continue;
    }
  }
};

const initialize = async () => {
  const drivesInMedia = await fs.readdir(DRIVE_MOUNT_DIRECTORY);

  for (const drive of drivesInMedia) {
    handleDriveMount(drive);
  }

  watch(DRIVE_MOUNT_DIRECTORY, (_, drive) => handleDriveMount(drive));
};

initialize();
