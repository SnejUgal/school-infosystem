import { Router } from "express";
import { getVideoStream, getSubtitleStream } from "./state/videos";

export const videoRouter = Router({
  caseSensitive: true,
  strict: true,
});

videoRouter.get(`/:id`, (request, response) => {
  const id = Number(request.params.id);
  const stream = getVideoStream(id);

  if (stream) {
    stream.pipe(response);
  } else {
    response.status(404).send();
  }
});

videoRouter.get(`/:id/subtitles`, (request, response) => {
  const id = Number(request.params.id);
  const stream = getSubtitleStream(id);

  if (stream) {
    stream.pipe(response);
  } else {
    response.status(404).send();
  }
});

export default videoRouter;
