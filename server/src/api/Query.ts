import { Context } from "../api";
import * as database from "../state/database";
import * as videos from "../state/videos";
import * as configuration from "../state/configuration";

export const isAuthenticated = (_: unknown, _args: {}, context: Context) =>
  context.isAuthenticated();
export const getVideoList = videos.getList;
export const getDrives = videos.getDrives;
export const getActiveRingSchedule = database.getActiveRingSchedule;
export const getAllRingSchedules = database.getAllRingSchedules;
export const getClassSchedule = () => configuration.classSchedule;
export const getReplacedClasses = () => configuration.replacedClasses;
