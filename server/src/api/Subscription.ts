import { Video } from "@infosystem/types/lib/videos";
import { RingSchedule } from "@infosystem/types/lib/ringSchedule";
import { ClassSchedule, ReplacedClasses } from "@infosystem/types/lib/config";
import events, {
  RING_SCHEDULES_UPDATE,
  ACTIVE_RING_SCHEDULE_UPDATE,
  VIDEOS_UPDATE,
  DRIVES_UPDATE,
  CLASS_SCHEDULE_UPDATE,
  REPLACED_CLASSES_UPDATE,
} from "../state/events";

export const watchVideoList = {
  resolve: (parent: Video[]) => parent,
  subscribe: () => events.asyncIterator(VIDEOS_UPDATE),
};

export const watchDrives = {
  resolve: (parent: string[]) => parent,
  subscribe: () => events.asyncIterator(DRIVES_UPDATE),
};

export const watchAllRingSchedules = {
  resolve: (parent: RingSchedule[]) => parent,
  subscribe: () => events.asyncIterator(RING_SCHEDULES_UPDATE),
};

export const watchActiveRingSchedule = {
  resolve: (parent: RingSchedule) => parent,
  subscribe: () => events.asyncIterator(ACTIVE_RING_SCHEDULE_UPDATE),
};

export const watchClassSchedule = {
  resolve: (parent: ClassSchedule[]) => parent,
  subscribe: () => events.asyncIterator(CLASS_SCHEDULE_UPDATE),
};

export const watchReplacedClasses = {
  resolve: (parent: ReplacedClasses[]) => parent,
  subscribe: () => events.asyncIterator(REPLACED_CLASSES_UPDATE),
};
