import { Context, OneTimeAuthentication } from "../api";
import * as path from "path";
import { randomBytes, createHash } from "crypto";
import {
  AuthenticationError,
  ApolloError,
  UserInputError,
} from "apollo-server-express";
import { RingSchedule, ClassRings } from "@infosystem/types/lib/ringSchedule";
import * as database from "../state/database";
import * as videos from "../state/videos";

interface SignIn {
  key: string;
}

export const signIn = async (_: unknown, args: SignIn, context: Context) => {
  // инвалидация предыдущего токена параллельно, если есть
  (async () => {
    const { cookies } = context.request;

    if (`token` in cookies && typeof cookies.token === `string`) {
      await database.invalidateToken(cookies.token);
    }
  })();

  const hashedKey = createHash(`sha256`).update(args.key).digest(`hex`);

  if (await database.isKeyValid(hashedKey)) {
    const token = randomBytes(32).toString(`hex`);

    await database.addNewToken(hashedKey, token);

    context.response.cookie(`token`, token, {
      httpOnly: true,
      maxAge: 1000 * 3600 * 24 * 365,
    });

    return true;
  }

  return false;
};

export const signOut = async (_: unknown, _args: {}, context: Context) => {
  const { cookies } = context.request;

  if (`token` in cookies && typeof cookies.token === `string`) {
    // инвалидация параллельно
    database.invalidateToken(cookies.token);
  }

  context.response.clearCookie(`token`, { httpOnly: true });

  return true;
};

interface Unmount extends OneTimeAuthentication {
  drive: string;
}

export const unmount = async (_: unknown, args: Unmount, context: Context) => {
  if (!(await context.isAuthenticated(args.key))) {
    throw new AuthenticationError(`Unathorized`);
  }

  if (path.basename(args.drive) !== args.drive) {
    throw new UserInputError(`Invalid drive name`);
  }

  try {
    await videos.unmountDrive(args.drive);
    return true;
  } catch (error: any) {
    throw new ApolloError(error.message, `UNMOUNT_ERROR`);
  }
};

type CreateRingSchedule = RingSchedule & OneTimeAuthentication;

export const createRingSchedule = async (
  _: unknown,
  args: CreateRingSchedule,
  context: Context,
) => {
  if (!(await context.isAuthenticated(args.key))) {
    throw new AuthenticationError(`Unathorized`);
  }

  try {
    return await database.createRingSchedule(args);
  } catch (error: any) {
    throw new UserInputError(error.message);
  }
};

interface EditRingSchedule extends OneTimeAuthentication {
  oldName: string;
  newName?: string;
  schedule?: ClassRings[];
}

export const editRingSchedule = async (
  _: unknown,
  args: EditRingSchedule,
  context: Context,
) => {
  if (!(await context.isAuthenticated(args.key))) {
    throw new AuthenticationError(`Unathorized`);
  }

  try {
    return await database.editRingSchedule(args.oldName, {
      name: args.newName,
      schedule: args.schedule,
    });
  } catch (error: any) {
    throw new UserInputError(error.message);
  }
};

interface SetActiveRingSchedule extends OneTimeAuthentication {
  name: string;
}

export const setActiveRingSchedule = async (
  _: unknown,
  args: SetActiveRingSchedule,
  context: Context,
) => {
  if (!(await context.isAuthenticated(args.key))) {
    throw new AuthenticationError(`Unathorized`);
  }

  try {
    return await database.setActiveRingSchedule(args.name);
  } catch (error: any) {
    throw new UserInputError(error.message);
  }
};

type DeleteRingSchedule = SetActiveRingSchedule;

export const deleteRingSchedule = async (
  _: unknown,
  args: DeleteRingSchedule,
  context: Context,
) => {
  if (!(await context.isAuthenticated(args.key))) {
    throw new AuthenticationError(`Unathorized`);
  }

  try {
    return await database.deleteRingSchedule(args.name);
  } catch (error: any) {
    throw new UserInputError(error.message);
  }
};
