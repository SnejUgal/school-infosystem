import express = require("express");
import cookieParser = require("cookie-parser");
import * as path from "path";
import { networkInterfaces } from "os";
import { config as dotenv } from "dotenv";
import { createServer } from "http";

import api from "./api";
import videoRouter from "./videoRouter";

dotenv();

const PORT = 2000;

const app = express();

app.use(cookieParser());
app.use(express.json());

const kioskPath = path.join(__dirname, `../../kiosk/build`);
app.use(`/kiosk`, express.static(kioskPath));

const controlPanelPath = path.join(__dirname, `../../controlPanel/build`);
app.use(`/controlPanel`, express.static(controlPanelPath));
app.use(`/controlPanel`, (_, response) => {
  response.sendFile(path.join(controlPanelPath, `index.html`));
});

const resourcesPath = path.join(__dirname, `../../resources`);
app.use(`/resources`, express.static(resourcesPath));

app.use(`/videos`, videoRouter);

app.use(`/api`, (request, response, next) => {
  const { cookies } = request;

  if (`token` in cookies && typeof cookies.token === `string`) {
    response.cookie(`token`, cookies.token, {
      httpOnly: true,
      maxAge: 1000 * 3600 * 24 * 365,
    });
  }

  next();
});

api.applyMiddleware({ app, path: `/api` });
const server = createServer(app);
api.installSubscriptionHandlers(server);

server.listen(PORT, `::`, () => {
  const interfaces = networkInterfaces();
  let ip = `[::]`;

  outer: for (const addresses of Object.values(interfaces)) {
    if (!addresses) {
      continue;
    }

    for (const { address, internal, family } of addresses) {
      if (internal) {
        continue;
      }

      if (family === `IPv4`) {
        ip = address;
      } else {
        ip = `[${address}]`;
      }
      break outer;
    }
  }

  console.log(
    `The server has started and is accepting connections over ${ip}:${PORT}…`,
  );
});
