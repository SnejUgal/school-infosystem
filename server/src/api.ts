import { ApolloServer, gql } from "apollo-server-express";
import { readFileSync } from "fs";
import { join } from "path";
import express = require("express");
import { createHash } from "crypto";

import * as Mutation from "./api/Mutation";
import * as Query from "./api/Query";
import * as Subscription from "./api/Subscription";

import * as database from "./state/database";

const typeDefs = gql(
  readFileSync(join(__dirname, `../../types/api.graphql`), `utf8`),
);

const resolvers = {
  Mutation,
  Query,
  Subscription,
};

export interface OneTimeAuthentication {
  key?: string;
}

export interface Context {
  request: express.Request;
  response: express.Response;
  connection?: object;
  isTokenValid(): Promise<boolean>;
  isAuthenticated(oneTimeKey?: string): Promise<boolean>;
  isKeyValid(key: string): Promise<boolean>;
}

export default new ApolloServer({
  typeDefs,
  resolvers,
  subscriptions: {
    path: `/api`,
  },
  context: ({ req: request, res: response, connection }): Context => ({
    connection,
    request,
    response,
    async isTokenValid() {
      if (typeof request.cookies.token !== `string`) {
        return false;
      }

      return database.isTokenValid(request.cookies.token);
    },
    async isAuthenticated(oneTimeKey?: string) {
      return (
        (await this.isTokenValid()) ||
        (oneTimeKey !== undefined && (await this.isKeyValid(oneTimeKey)))
      );
    },
    async isKeyValid(key: string) {
      const hashedKey = createHash(`sha256`).update(key).digest(`hex`);

      return database.isKeyValid(hashedKey);
    },
  }),
});
