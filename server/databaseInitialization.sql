create type ring as range (
    subtype = time
);

create table "ringSchedules" (
    name text unique not null,
    schedule ring[] not null
);

create table state (
    "activeRingSchedule" text not null
);

create table "authenticationKeys" (
    -- Хеш SHA-256
    key text not null unique,
    name text not null unique
);

create table "authenticationTokens" (
    key text not null,
    token text not null unique
);

-- Немного данных, чтобы БД была в валидном состоянии
insert
    into "ringSchedules" (name, schedule)
    values ('Обычное', '{"[8:30,9:10)"}');

insert
    into state ("activeRingSchedule")
    values ('Обычное');
