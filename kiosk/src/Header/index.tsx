import "./style.scss";
import React from "react";
import { ParsedRing, ParsedRings } from "../types";
import { format, formatDistance, isBefore, isWithinInterval } from "date-fns";
import { ru } from "date-fns/locale";

interface Props {
  ringSchedule: ParsedRings;
  time: Date;
}

type CurrentClass =
  | {
      nth: number;
      next: ParsedRing;
    }
  | {
      nth: number;
      now: ParsedRing;
    }
  | null;

const getCurrentClass = (time: Date, schedule: ParsedRings): CurrentClass => {
  for (const [index, item] of schedule.entries()) {
    if (isBefore(time, item.start)) {
      return {
        nth: index + 1,
        next: item,
      };
    }

    if (isWithinInterval(time, item)) {
      return {
        nth: index + 1,
        now: item,
      };
    }
  }

  return null;
};

const Header = ({ ringSchedule, time }: Props) => {
  const currentClass = getCurrentClass(time, ringSchedule);

  let currentClassTitle;
  let nextClass;

  if (currentClass) {
    if (`now` in currentClass) {
      currentClassTitle = `Сейчас ${currentClass.nth}-й урок`;

      const breakTime = formatDistance(currentClass.now.end, time, {
        includeSeconds: true,
        addSuffix: true,
        locale: ru,
      });
      nextClass = `Перемена ${breakTime}`;
    } else {
      currentClassTitle = `Сейчас перемена`;

      const classTime = formatDistance(currentClass.next.start, time, {
        includeSeconds: true,
        addSuffix: true,
        locale: ru,
      });
      nextClass = `${currentClass.nth}-й урок ${classTime}`;
    }
  }

  return (
    <header className="header">
      <img
        className="header_logo"
        src="/resources/logo.png"
        alt="Логотип школы"
      />
      <time className="header_datetime">
        <span className="header_date">
          {format(time, `d MMMM`, { locale: ru })}
        </span>
        <span className="time header_time">
          {format(time, `HH`)}
          <span className="time_colon">:</span>
          {format(time, `mm`)}
        </span>
      </time>
      {currentClass && (
        <div className="header_class">
          <h2 className="header_currentClass">{currentClassTitle}</h2>
          <p className="header_nextClass">{nextClass}</p>
        </div>
      )}
    </header>
  );
};

export default Header;
