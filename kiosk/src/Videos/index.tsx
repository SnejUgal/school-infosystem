import "./style.scss";
import React, { useRef, useEffect } from "react";
import { ParsedRings } from "../types";
import useVideos from "./useVideos";
import { useQuery, useSubscription } from "@apollo/client";
import { Video } from "@infosystem/types/lib/videos";
import { loader } from "graphql.macro";
import {
  addSeconds,
  differenceInMilliseconds,
  isBefore,
  subSeconds,
} from "date-fns";

const FADE_DURATION = 5; // секунд

const calculateMediaVolume = (time: Date, schedule: ParsedRings) => {
  for (const item of schedule.values()) {
    const fadeInStart = subSeconds(item.start, FADE_DURATION);
    if (isBefore(time, fadeInStart)) {
      return 1;
    }

    if (isBefore(time, item.start)) {
      const seconds = differenceInMilliseconds(time, fadeInStart) / 1000;
      return 1 - seconds / FADE_DURATION;
    }

    if (isBefore(time, item.end)) {
      return 0;
    }

    const fadeOutEnd = addSeconds(item.end, FADE_DURATION);
    if (isBefore(time, fadeOutEnd)) {
      const seconds = differenceInMilliseconds(time, item.end) / 1000;
      return seconds / FADE_DURATION;
    }
  }

  return 1;
};

interface VideoProps {
  video: string;
  volume: number;
  subtitles?: string;
  onEnd(): void;
}

const VideoWrapper = ({ video, volume, subtitles, onEnd }: VideoProps) => {
  const videoRef = useRef<HTMLVideoElement>(null);

  useEffect(() => {
    if (!videoRef.current) {
      return;
    }

    videoRef.current.volume = volume;
  }, [videoRef, volume]);

  return (
    <video
      ref={videoRef}
      className="content_inner"
      src={video}
      autoPlay={true}
      onEnded={onEnd}
    >
      {subtitles && (
        <track kind="subtitles" src={subtitles} srcLang="ru" default={true} />
      )}
    </video>
  );
};

const VIDEOS_QUERY = loader("./queries/query.graphql");
const VIDEOS_SUBSCRIPTION = loader("./queries/subscription.graphql");

interface Data {
  videos: Video[];
}

interface Props {
  ringSchedule: ParsedRings;
  time: Date;
}

const Videos = ({ time, ringSchedule }: Props) => {
  const { key, video, nextVideo, setVideos } = useVideos();

  useQuery<Data>(VIDEOS_QUERY, {
    onCompleted: ({ videos }) => setVideos(videos),
  });

  useSubscription<Data>(VIDEOS_SUBSCRIPTION, {
    onSubscriptionData: ({ subscriptionData: { data } }) => {
      if (data?.videos) {
        setVideos(data.videos);
      }
    },
  });

  let contentElement = null;

  if (video) {
    const subtitles = video.hasSubtitles
      ? `/videos/${video.id}/subtitles`
      : undefined;

    contentElement = (
      <VideoWrapper
        key={key}
        video={`/videos/${video.id}`}
        volume={calculateMediaVolume(time, ringSchedule)}
        subtitles={subtitles}
        onEnd={nextVideo}
      />
    );
  }

  return <section className="content">{contentElement}</section>;
};

export default Videos;
