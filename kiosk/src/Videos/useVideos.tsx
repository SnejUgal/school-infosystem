import { useState, useCallback } from "react";
import { Video } from "@infosystem/types/lib/videos";

const useVideos = () => {
  const [key, setKey] = useState(Number.MIN_SAFE_INTEGER);
  const [videos, setVideos] = useState<Video[]>([]);
  const [order, setOrder] = useState<number | null>(null);

  const updateVideos = useCallback(
    (newVideos: Video[]) => {
      if (newVideos.length === 0) {
        setOrder(null);
        setVideos(newVideos);
        return;
      }

      const sortedVideos = [...newVideos].sort((a, b) => a.order - b.order);

      const distances = sortedVideos.map(video => ({
        distance: Math.abs(video.order - (order ?? 0)),
        order: video.order,
      }));

      distances.sort((left, right) => {
        // Ищем видео, которое ближе всего по порядку...
        const distance = left.distance - right.distance;

        if (distance !== 0) {
          return distance;
        }

        // ...но если два видео одинаково близки, то предпочитаем то,
        // которое с большим порядковым номером
        return right.order - left.order;
      });

      const { order: nearestOrder } = distances[0];

      setOrder(nearestOrder);
      setVideos(sortedVideos);
    },
    [order],
  );

  const nextVideo = () => {
    if (order === null) {
      return;
    }

    const nextVideo = videos.find(video => video.order > order);

    setOrder(nextVideo?.order ?? videos[0].order);
    setKey(key + 1);
  };

  return {
    key,
    video:
      order === null ? undefined : videos.find(video => video.order === order),
    nextVideo,
    setVideos: updateVideos,
  };
};

export default useVideos;
