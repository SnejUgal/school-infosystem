import "./style.scss";

import React from "react";
import { ApolloProvider } from "@apollo/client";

import Classes from "../Classes";
import Videos from "../Videos";
import Header from "../Header";
import useTime from "../useTime";
import Ring from "../Ring";
import useRingSchedule from "./useRingSchedule";
import client from "./client";

const App = () => {
  const time = useTime();
  const ringSchedule = useRingSchedule({ time, client });

  return (
    <ApolloProvider client={client}>
      <Header ringSchedule={ringSchedule} time={time} />
      <Classes ringSchedule={ringSchedule} time={time} />
      <Videos ringSchedule={ringSchedule} time={time} />
      <Ring ringSchedule={ringSchedule} time={time} />
    </ApolloProvider>
  );
};

export default App;
