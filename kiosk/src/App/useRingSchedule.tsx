import { ClassRings } from "@infosystem/types/lib/ringSchedule";
import { useState, useMemo } from "react";
import { useQuery, useSubscription, ApolloClient } from "@apollo/client";
import { loader } from "graphql.macro";
import { formatISO, set } from "date-fns";

const SCHEDULE_QUERY = loader("./queries/ringScheduleQuery.graphql");
const SCHEDULE_SUBSCRIPTION = loader(
  "./queries/ringScheduleSubscription.graphql",
);

interface Data {
  ringSchedule: { schedule: ClassRings[] };
}

interface Props<C> {
  time: Date;
  client: ApolloClient<C>;
}

function useRingSchedule<C extends object>({ time, client }: Props<C>) {
  const [unparsedSchedule, setUnparsedSchedule] = useState<ClassRings[]>([]);

  useQuery<Data>(SCHEDULE_QUERY, {
    client,
    onCompleted: ({ ringSchedule }) =>
      setUnparsedSchedule(ringSchedule.schedule),
  });

  useSubscription<Data>(SCHEDULE_SUBSCRIPTION, {
    client,
    onSubscriptionData: ({ subscriptionData: { data } }) => {
      if (data?.ringSchedule) {
        setUnparsedSchedule(data.ringSchedule.schedule);
      }
    },
  });

  const date = formatISO(time, { representation: `date` });
  const ringSchedule = useMemo(() => {
    const startOfDay = new Date(date);

    return unparsedSchedule.map(({ start, end }) => ({
      start: set(startOfDay, { hours: start.hour, minutes: start.minute }),
      end: set(startOfDay, { hours: end.hour, minutes: end.minute }),
    }));
  }, [unparsedSchedule, date]);

  return ringSchedule;
}

export default useRingSchedule;
