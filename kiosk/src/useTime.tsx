import { useState, useEffect } from "react";

const useTime = () => {
  const [timestamp, setTimestamp] = useState(new Date());

  useEffect(() => {
    setInterval(() => setTimestamp(new Date()), 1000);
  }, []);

  return timestamp;
};

export default useTime;
