import { isWithinInterval } from "date-fns";
import React from "react";
import { ParsedRings } from "../types";

interface Props {
  ringSchedule: ParsedRings;
  time: Date;
}

const hasClassNow = (time: Date, ringSchedule: ParsedRings): boolean => {
  return [...ringSchedule.values()].some(item => isWithinInterval(time, item));
};

const Ring = ({ ringSchedule, time }: Props) => {
  return (
    <audio
      key={Number(hasClassNow(time, ringSchedule))}
      src="./ringSound.oga"
      autoPlay={true}
    />
  );
};

export default Ring;
