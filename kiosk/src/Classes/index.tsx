import "./style.scss";
import React, { useMemo, useState } from "react";
import classNames from "classnames";
import {
  ClassSchedule,
  ReplacedClasses,
  ScheduleItem,
  ClassScheduleWeek,
} from "@infosystem/types/lib/config";
import { ParsedRings } from "../types";
import { cloneDeep } from "lodash";
import { useSubscription, useQuery } from "@apollo/client";
import { loader } from "graphql.macro";
import { formatISO, isBefore } from "date-fns";

const WEEKDAYS: (keyof ClassScheduleWeek)[] = [
  `monday`,
  `tuesday`,
  `wednesday`,
  `thursday`,
  `friday`,
  `saturday`,
];

interface ScheduleToday {
  [grade: string]: ScheduleItem[];
}

const getCurrentClass = (time: Date, rings: ParsedRings) => {
  const index = rings.findIndex(item => isBefore(time, item.end));

  return index === -1 ? null : index;
};

const getTodaySchedule = (
  weekday: keyof ClassScheduleWeek,
  date: string,
  regular: ClassSchedule[],
  replaced: ReplacedClasses[],
) => {
  const finalSchedule: ScheduleToday = {};

  if (!weekday) {
    return finalSchedule;
  }

  for (const { grade, schedule } of regular) {
    finalSchedule[grade] = cloneDeep(schedule[weekday]);
  }

  const replacedToday = replaced.find(day => day.date === date);

  if (replacedToday) {
    for (const { grade, classes } of replacedToday.grades) {
      for (const { index, class: replacingClass } of classes) {
        finalSchedule[grade][index] = replacingClass;
      }
    }
  }

  return finalSchedule;
};

const INITIAL_QUERY = loader("./queries/initialQuery.graphql");
const CLASS_SCHEDULE_SUBSCRIPTION = loader(
  "./queries/classScheduleSubscription.graphql",
);
const REPLACED_CLASSES_SUBSCRIPTION = loader(
  "./queries/replacedClassesSubscription.graphql",
);

interface Data {
  classSchedule: ClassSchedule[];
  replacedClasses: ReplacedClasses[];
}

interface Props {
  ringSchedule: ParsedRings;
  time: Date;
}

const Classes = ({ ringSchedule, time }: Props) => {
  const [classSchedule, setClassSchedule] = useState<ClassSchedule[]>([]);
  const [replacedClasses, setReplacedClasses] = useState<ReplacedClasses[]>([]);

  useQuery<Data>(INITIAL_QUERY, {
    onCompleted: ({ classSchedule, replacedClasses }) => {
      setClassSchedule(classSchedule);
      setReplacedClasses(replacedClasses);
    },
  });

  useSubscription<Pick<Data, "classSchedule">>(CLASS_SCHEDULE_SUBSCRIPTION, {
    onSubscriptionData: ({ subscriptionData: { data } }) => {
      if (data?.classSchedule) {
        setClassSchedule(data.classSchedule);
      }
    },
  });

  useSubscription<Pick<Data, "replacedClasses">>(
    REPLACED_CLASSES_SUBSCRIPTION,
    {
      onSubscriptionData: ({ subscriptionData: { data } }) => {
        if (data?.replacedClasses) {
          setReplacedClasses(data.replacedClasses);
        }
      },
    },
  );

  const currentClass = getCurrentClass(time, ringSchedule);
  const weekday = WEEKDAYS[time.getDay() - 1];
  const date = formatISO(time, { representation: `date` });
  const todaySchedule = useMemo(
    () => getTodaySchedule(weekday, date, classSchedule, replacedClasses),
    [weekday, date, classSchedule, replacedClasses],
  );

  const classes = useMemo(() => {
    if (currentClass == null) {
      return [];
    }

    const classes = Object.entries(todaySchedule)
      .filter(([_, x]) => x.length > 0)
      .map(([grade, classes]) => {
        const firstClass = classes.findIndex(item => `subject` in item);
        const lastClass =
          classes.length -
          1 -
          [...classes].reverse().findIndex(item => `subject` in item);
        const isAtSchool =
          firstClass <= currentClass && currentClass <= lastClass;

        if (!isAtSchool) {
          return (
            <tr key={grade} className="class -noClass">
              <th className="class_grade">{grade}</th>
              <th className="class_subject" />
            </tr>
          );
        }

        const item = classes[currentClass];

        if (!(`subject` in item)) {
          return (
            <tr key={grade} className="class">
              <th className="class_grade">{grade}</th>
              <td className="class_subject -isWindow">Окно</td>
            </tr>
          );
        }

        const subjectClassName = classNames(`class_subject`, {
          "-isElective": item.isElective,
        });

        return (
          <tr key={grade} className="class">
            <th className="class_grade">{grade}</th>
            <td className={subjectClassName}>{item.subject}</td>
            {item.cabinet && <td className="class_cabinet">{item.cabinet}</td>}
          </tr>
        );
      });

    return classes;
  }, [todaySchedule, currentClass]);

  return (
    <table className="classes">
      <tbody>{classes}</tbody>
    </table>
  );
};

export default Classes;
