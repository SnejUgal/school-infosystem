import { Interval } from "date-fns";

export type ParsedRing = Interval;
export type ParsedRings = Interval[];
