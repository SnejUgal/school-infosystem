import React from "react";
import ReactDom from "react-dom";
import App from "./App";

if (process.env.NODE_ENV === `production`) {
  document.documentElement.classList.add(`-hideCursor`);
}

const root = document.querySelector(`main`);
ReactDom.render(<App />, root);
