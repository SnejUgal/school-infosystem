# Киоск

Киоск показывает текущие уроки, время и различный видеоконтент.

![Фотография киоска в Богашёвской школе](../readmeImages/kiosk.jpg)

## Сборка

Запуск сервера для разработки (с перезагрузкой при изменении файлов):

```bash
npm start
```

Сборка для продакшена (для раздачи сервером):

```bash
npm run build
```

С помощью [`kioskApp`] можно запускать киоск через Electron как отдельное
приложение, в том числе при запуске ОС.

[`kioskApp`]: ../kioskApp/
