const { app, BrowserWindow } = require(`electron`);

let window;

app.on(`ready`, () => {
  window = new BrowserWindow({
    fullscreen: true,
    autoHideMenuBar: true,
  });

  window.loadURL(process.argv[2] || `http://[::1]:2000/kiosk`);

  window.on(`closed`, () => {
    window = null;
  });
});

app.on(`window-all-closed`, () => app.quit());
