export interface Ring {
  hour: number;
  minute: number;
}

export interface ClassRings {
  start: Ring;
  end: Ring;
}

export interface RingSchedule {
  name: string;
  schedule: ClassRings[];
}
