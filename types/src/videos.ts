export interface Video {
  id: number;
  order: number;
  hasSubtitles: boolean;
}
