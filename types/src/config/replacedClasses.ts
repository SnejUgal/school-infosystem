import { ScheduleItem, isScheduleItem } from "./classSchedule";

export interface ReplacedClass {
  index: number;
  class: ScheduleItem;
}

export interface ConfigReplacedClasses {
  [index: string]: ScheduleItem;
}

export const isConfigReplacedClasses = (
  schedule: unknown,
): schedule is ConfigReplacedClasses => {
  if (typeof schedule !== `object` || schedule === null) {
    return false;
  }

  return Object.values(schedule).every(isScheduleItem);
};

export interface GradeReplacedClasses {
  grade: string;
  classes: ReplacedClass[];
}

export interface ConfigGradeReplacedClasses {
  [grade: string]: ConfigReplacedClasses;
}

export const isConfigGradeReplacedClasses = (
  day: unknown,
): day is ConfigGradeReplacedClasses => {
  if (typeof day !== `object` || day === null) {
    return false;
  }

  return Object.values(day).every(isConfigReplacedClasses);
};

export interface ReplacedClasses {
  date: string;
  grades: GradeReplacedClasses[];
}

export interface ConfigAllReplacedClasses {
  [date: string]: ConfigGradeReplacedClasses;
}

export const isConfigAllReplacedClasses = (
  replacedClasses: unknown,
): replacedClasses is ConfigAllReplacedClasses => {
  if (typeof replacedClasses !== `object` || replacedClasses === null) {
    return false;
  }

  return Object.values(replacedClasses).every(isConfigGradeReplacedClasses);
};
