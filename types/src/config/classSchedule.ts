export interface Subject {
  subject: string;
  cabinet?: string;
  isElective?: true;
}

export const isSubject = (subject: unknown): subject is Subject => {
  if (typeof subject !== `object` || subject === null) {
    return false;
  }

  const isSubjectValid =
    // @ts-ignore it exists
    `subject` in subject && typeof subject.subject === `string`;
  const isCabinetValid =
    // @ts-ignore it exists
    `subject` in subject ? typeof subject.subject === `string` : true;
  const isIsElectiveValid =
    // @ts-ignore it exists
    `isElective` in subject ? subject.isElective === true : true;

  return isSubjectValid && isCabinetValid && isIsElectiveValid;
};

export type Window = {};

export const isWindow = (window: unknown): window is Window => {
  if (typeof window !== `object` || window === null) {
    return false;
  }

  return Object.keys(window).length === 0;
};

export type ScheduleItem = Subject | Window;

export const isScheduleItem = (item: unknown): item is ScheduleItem => {
  return isWindow(item) || isSubject(item);
};

export type WeekdaySchedule = ScheduleItem[];

export const isWeekdaySchedule = (
  schedule: unknown,
): schedule is WeekdaySchedule => {
  if (Array.isArray(schedule)) {
    return schedule.every(isScheduleItem);
  }

  return false;
};

export interface ClassScheduleWeek {
  monday: WeekdaySchedule;
  tuesday: WeekdaySchedule;
  wednesday: WeekdaySchedule;
  thursday: WeekdaySchedule;
  friday: WeekdaySchedule;
  saturday: WeekdaySchedule;
}

export const isSchedulePerWeekdays = (
  schedule: unknown,
): schedule is ClassScheduleWeek => {
  if (typeof schedule !== `object` || schedule === null) {
    return false;
  }

  const arePropertiesValid: { [key: string]: boolean } = {
    monday: false,
    tuesday: false,
    wednesday: false,
    thursday: false,
    friday: false,
    saturday: false,
  };

  for (const [key, weekday] of Object.entries(schedule)) {
    arePropertiesValid[key] = isWeekdaySchedule(weekday);
  }

  return Object.values(arePropertiesValid).every(x => x);
};

export interface ClassSchedule {
  grade: string;
  schedule: ClassScheduleWeek;
}

export interface ConfigClassSchedule {
  [grade: string]: ClassScheduleWeek;
}

export const isConfigClassSchedule = (
  classSchedule: unknown,
): classSchedule is ConfigClassSchedule => {
  if (typeof classSchedule !== `object` || classSchedule === null) {
    return false;
  }

  return Object.values(classSchedule).every(isSchedulePerWeekdays);
};
