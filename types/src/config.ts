import { ConfigClassSchedule } from "./config/classSchedule";
import { ConfigReplacedClasses } from "./config/replacedClasses";

export * from "./config/classSchedule";
export * from "./config/replacedClasses";

export interface Config {
  classSchedule: ConfigClassSchedule;
  replacedClasses: ConfigReplacedClasses;
}
