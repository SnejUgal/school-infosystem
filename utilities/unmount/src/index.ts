#!/usr/bin/env node
import ApolloClient, { gql } from "apollo-boost";
import fetch from "cross-fetch";
import read = require("read");

const askKey = () =>
  new Promise<string>((resolve, reject) => {
    read({ prompt: `Ключ авторизации: `, silent: true }, (error, key) =>
      key ? resolve(key) : reject(error),
    );
  });

(async () => {
  const drive = process.argv[2];

  if (!drive) {
    console.error(`Не указано название носителя.`);
    process.exit(1);
  }

  const client = new ApolloClient({
    uri: `http://[::1]:2000/api`,
    fetch,
    onError: () => {},
  });

  while (true) {
    let key;
    try {
      key = await askKey();
    } catch {
      return;
    }

    const { errors } = await client.mutate({
      mutation: gql`
        mutation unmount($key: String!, $drive: String!) {
          isUnmounted: unmount(key: $key, drive: $drive)
        }
      `,
      variables: { drive, key },
      errorPolicy: `all`,
    });

    if (!errors) {
      return;
    }

    const [error] = errors;

    if (error.extensions!.code === `UNATHORIZED`) {
      console.error(`Неверный ключ аутентификации. Попробуйте снова.`);
      continue;
    }

    if (error.extensions!.code === `UNMOUNT_ERROR`) {
      console.error(`Не удалось размонтировать носитель.`);
      return;
    }
    if (error.extensions!.code === `BAD_USER_INPUT`) {
      console.error(`Невалидное название носителя.`);
      return;
    }

    console.error(`Ошибка сервера: ${error.message}`);
    return;
  }
})();
