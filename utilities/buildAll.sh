#!/bin/bash

echo "Сборка сервера..."
cd `dirname "$0"`/../server
npm install
npm run build

echo "Сборка киоска..."
cd ../kiosk
npm install
npm run build

echo "Установка Electron для киоск-приложения..."
cd ../kioskApp
npm install

echo "Сборка панели управления..."
cd ../controlPanel
npm install
npm run build

echo "Сборка и установка infosystem-unmount..."
cd ../utilities/unmount
npm install
npm run build
npm install --global .
