#!/bin/bash

# Загружаем nvm, поскольку этот скрипт исполняется юнитом systemd
\. ~/.config/nvm/nvm.sh

# Запускаем сервер
cd `dirname "$0"`/../../server
npm run start:production
